import React, {Component} from "react";
import ReactDOM from "react-dom";
import LoginComponent from "../common/Login";
import {getUserDetails} from "../common/LoginUtils";
import UserButton from "../common/UserButton";

export default class Header extends Component{
    constructor(props){
        super(props);

        this.headerElem = null;

        this.state = {
            loginUser:null,
            showLogin:false
        }
    }

    componentDidMount(){
        document.body.addEventListener("mousewheel", this.scrollHandler.bind(this));
        document.body.addEventListener("DOMMouseScroll", this.scrollHandler.bind(this));
        this.checkUserLoginStatus();
    }

    checkUserLoginStatus(){
        this.setState({
            loginUser:getUserDetails()
        });
    }

    scrollHandler(){
        var self = this;
        var headerElem = ReactDOM.findDOMNode(self.headerElem);
        var scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop)
        if(scrollTop > 60){
            headerElem.classList.add("active");
        } else {
            headerElem.classList.remove("active");
        }
    }

    componentWillUnmount(){
        document.body.removeEventListener("mousewheel", this.scrollHandler);
        document.body.removeEventListener("DOMMouseScroll", this.scrollHandler);   
    }

    render(){
        return(
            <div className="header-wrap" ref={node => this.headerElem = node}>
                <div className="container">
                    <div className="logo"/>
                    <ul>
                        <li>
                            <a className="nav-text">About Us</a>
                        </li>
                        <li>
                            <a className="nav-text">Careers</a>
                        </li>
                        <li>
                            <UserButton
                                theme={"light"}
                                userLoggedOut={()=>{
                                    this.checkUserLoginStatus();
                                }}
                                userLoggedIn={()=>{
                                    this.checkUserLoginStatus();
                                }}/>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}