import React from "react";
import Constants from "../common/constants";
import axios from "axios";
import Cookie from "../common/Cookie";
import AES from "crypto-js/aes";

export default class LoginComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isApiCallInProgress:false,
            showSignUp:false,
            username:"",
            password:"",
            signupUsername:"",
            signupPassword:"",
            signupPasswordConfirm:"",
            signupEmail:"",
            signupPhone:"",
            isRegisterFormComplete:false,
            showVerificationRequiredMessage:false
        }

        this.notVerifiedUserId = "";
    }

    isFormEmpty(){
        if(this.username && this.password){
            if(this.password.value !== "" && this.username.value !== ""){
                return false
            };
        }
        return true;
    }

    login(){
        this.setState({
            isApiCallInProgress:true,
            showVerificationEmailSent:false,
            showVerificationRequiredMessage:false
        },()=>{
            axios({
                method:"POST",
                url:Constants.API_BASE_URL + "/users/login",
                data:{
                    email:this.state.username,
                    password:this.state.password
                }
            }).then(response => {
                Cookie.setCookie(Constants.USERDATA_COOKIE,AES.encrypt(JSON.stringify(response.data), Constants.ENC));
                this.setState({
                    isApiCallInProgress:false,
                },()=>{
                    this.props.onLoginSuccess();
                })
            }).catch(err => {
                this.setState({
                    isApiCallInProgress:false
                },()=>{
                    if(err.response.data.error && err.response.data.error.code){
                        if(err.response.data.error.code == "LOGIN_FAILED_EMAIL_NOT_VERIFIED"){
                            this.notVerifiedUserId = err.response.data.error.details.userId;
                            this.setState({
                                showVerificationRequiredMessage:true,
                            })
                        }
                    }
                })
            });
        })
        
    }

    registerUser(){
        this.setState({
            isApiCallInProgress:true,
            showVerificationEmailSent:false,
            showVerificationRequiredMessage:false
        },()=>{
            axios({
                method:"POST",
                url:Constants.API_BASE_URL + "/users",
                data:{
                    username:this.state.signupUsername,
                    email:this.state.signupEmail,
                    password:this.state.signupPassword,
                    public:true
                }
            }).then(response => {
                console.log(response.data);
                this.setState({
                    isApiCallInProgress:false,
                    showVerificationEmailSent:true,
                    showSignUp:false
                })
            }).catch(err => {
                console.log(err);
                this.setState({
                    isApiCallInProgress:false
                })
            })
        })
    }

    sendVerificationMail(userIdToVerify){
        this.setState({
            isApiCallInProgress:true,
            showVerificationEmailSent:false,
            showVerificationRequiredMessage:false
        },()=>{
            axios({
                method:"POST",
                url:Constants.API_BASE_URL + "/users/" + userIdToVerify + "/verify",
            }).then(response =>{
                this.setState({
                    isApiCallInProgress:false,
                    showVerificationEmailSent:true
                })
            }).catch(err => {
                console.log(err);
                this.setState({
                    isApiCallInProgress:false
                });
            })
        })
    }

    render(){
        return(
            <div className="login-backdrop">
                <div className={`login-container` + (this.state.isApiCallInProgress ? " fade" : "")}>
                    <div className="close-btn" onClick={()=>{
                        this.props.onCloseButtonClicked();
                    }}/>
                    
                    <div className="mask">
                        <img src={require("../images/loader.svg")} className="loader"/>
                    </div>
                    {
                        this.state.showSignUp ? 
                        <div className="wrap">
                            <div className="signup-form">
                                <h2 className="form-title">Sign up</h2>
                                <form name="register-form" onSubmit={(e)=>{
                                    e.preventDefault();
                                    e.stopPropagation();
                                    this.registerUser();
                                }} autoComplete="off">
                                    <div className="form-group">
                                        <label htmlFor="username">
                                            <img src={require("../images/user.svg")}/>
                                        </label>
                                        <input type="text" name="username" placeholder="User Name" value={this.state.signupUsername} onChange={(e)=>{
                                            this.setState({
                                                signupUsername:e.target.value
                                            })
                                        }}/>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="emailsignup">
                                            <img src={require("../images/envelope-black.svg")}/>
                                        </label>
                                        <input type="text" name="emailsignup" placeholder="Your Email" value={this.state.signupEmail} onChange={(e)=>{
                                            this.setState({
                                                signupEmail:e.target.value
                                            })
                                        }}/>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="passwordsignup">
                                            <img src={require("../images/password.svg")}/>
                                        </label>
                                        <input type="password" name="passwordsignup" placeholder="Password" value={this.state.signupPassword} onChange={(e)=>{
                                            this.setState({
                                                signupPassword:e.target.value
                                            })
                                        }}/>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="rpasswordsignup">
                                            <img src={require("../images/password-confirm.svg")}/>
                                        </label>
                                        <input type="password" name="rpasswordsignup" placeholder="Confirm Password" value={this.state.signupPasswordConfirm} onChange={(e)=>{
                                            this.setState({
                                                signupPasswordConfirm:e.target.value
                                            })
                                        }}/>
                                    </div>
                                    <div className="form-group" style={{display:"none"}}>
                                        <label htmlFor="phonesignup">
                                            <img src={require("../images/call.svg")}/>
                                        </label>
                                        <input type="text" name="phonesignup" placeholder="Phone Number (Optional)" value={this.state.signupPhone} onChange={(e)=>{
                                            this.setState({
                                                signupPhone:e.target.value
                                            })
                                        }}/>
                                    </div>

                                    <div className="btn-wrap">
                                        <button type="submit" className="btnl">Register</button>
                                    </div>
                                </form>
                            </div>
                            <div className="simage sup">
                                <a className="txtlink linkbtn" onClick={()=>{
                                    this.setState({showSignUp:false})
                                }}>I am already a member</a>
                            </div>
                        </div>   
                        :
                        <div className="wrap">
                            <div className="simage login">
                                <a className="txtlink linkbtn" onClick={()=>{
                                    this.setState({showSignUp:true})
                                }}>Create an account</a>
                            </div>
                            <div className="signup-form">
                                <h2 className="form-title">Login</h2>
                                <form name="login-form" onSubmit={(e)=>{
                                    e.preventDefault();
                                    e.stopPropagation();
                                    this.login();
                                }} autoComplete="off" >
                                    <div className="form-group">
                                        <label htmlFor="username">
                                            <img src={require("../images/user.svg")}/>
                                        </label>
                                        <input 
                                            type="text" 
                                            name="username" 
                                            value={this.state.username} 
                                            placeholder="email" 
                                            onChange={(e)=>{
                                                this.setState({
                                                    username:e.target.value
                                                })}}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="password">
                                            <img src={require("../images/password.svg")}/>
                                        </label>
                                        <input type="password" name="password" value={this.state.password} placeholder="password" onChange={(e)=>{
                                            this.setState({
                                                password:e.target.value
                                            })
                                        }}/>
                                    </div>

                                    <div className="btn-wrap">
                                        <button type="submit" className="btnl" disabled={this.state.username == "" || this.state.password == ""}>Login</button>
                                    </div>
                                </form>

                                {
                                    this.state.showVerificationRequiredMessage ? 
                                    <div className="info-message">
                                        <div className="info-title">Account not verified</div>
                                        <p>Verify your account first by following the instructions sent to your email.</p>
                                        <p>Click <span className="link" onClick={()=>{
                                            this.sendVerificationMail(this.notVerifiedUserId);
                                        }}>here</span> to resend the verification email again.</p>
                                    </div>
                                    :
                                    null
                                }
                                {
                                    this.state.showVerificationEmailSent ? 
                                    <div className="info-message">
                                        <div className="info-title">Verification Mail sent</div>
                                        <p>Please check your email for instructions to verify your account</p>
                                        {/* <p>Didn't get the email? <span className="link" onClick={()=>{
                                            if(this.notVerifiedUserId !== ""){
                                                this.sendVerificationMail(this.notVerifiedUserId);
                                            } else {

                                            }
                                        }}>Resend </span> now</p> */}
                                    </div>
                                    :
                                    null
                                }
                            </div>
                        </div>
                    }
                </div>
            </div>
        )
    }
}