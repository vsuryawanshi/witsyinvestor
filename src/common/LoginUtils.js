import Cookie from "./Cookie";
import Constants from "../common/constants";
import axios from "axios";
import CryptoJS from "crypto-js";

export const isLoggedIn = () => {
    let savedCookie = Cookie.getCookie(Constants.LOGIN_COOKIE);
    if(savedCookie == null){
        return false;
    }
    return true;
}

export const getUserDetails = () => {
    var lc = Cookie.getCookie(Constants.USERDATA_COOKIE);
    if(lc !== null){
        var bytes = CryptoJS.AES.decrypt(lc.toString(), Constants.ENC);
        return bytes.toString(CryptoJS.enc.Utf8);
    } else {
        return null;
    }
}

export const getUser = (callback) => {
    var lc = Cookie.getCookie(Constants.USERDATA_COOKIE);
    if(lc !== null){
        var bytes = CryptoJS.AES.decrypt(lc.toString(), Constants.ENC);
        lc = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        var currentUserDetails = Cookie.getCookie(Constants.USERPDATA);
        if(currentUserDetails == null){
            axios({
                method:"GET",
                url:Constants.API_BASE_URL + "/users/" + lc.userId,
                headers:{
                    Authorization:lc.id
                }
            }).then(response => {
                Cookie.setCookie(Constants.USERPDATA,CryptoJS.AES.encrypt(JSON.stringify(response.data),Constants.ENC));
                callback(response.data);
            }).catch(err =>{
                console.log(err);
                callback(null);
            })
        } else {
            var bytes = CryptoJS.AES.decrypt(currentUserDetails.toString(), Constants.ENC);
            callback(JSON.parse(bytes.toString(CryptoJS.enc.Utf8)));
        }
    } else {
        callback(null);
    }
}

export const removeUserDetails = () => {
    Cookie.eraseCookie(Constants.LOGIN_COOKIE);
    Cookie.eraseCookie(Constants.USERDATA_COOKIE);
    Cookie.eraseCookie(Constants.USERPDATA);
}

export const logoutUserFromServer = (callback) => {
    var encrypted = Cookie.getCookie(Constants.USERDATA_COOKIE);
    var bytes = CryptoJS.AES.decrypt(encrypted.toString(),Constants.ENC);
    var ud = JSON.parse(bytes.toString(CryptoJS.enc.Utf8))
    var at = ud.id;
    axios({
        method:"POST",
        url:Constants.API_BASE_URL + "/users/logout?access_token=" + at
    }).then(() => {
        removeUserDetails();
        callback(null);
    }).catch(err => {
        console.log(err);
        callback(err);
    })
}