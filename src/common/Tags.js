import axios from "axios";
import Constants from "../common/constants";


export const getTags = (value, callback) => {
    let filter = {
        where:{
            tag:{
                like:value,
                options:"i"
            }
        }
    }
    axios({
        method:"GET",
        url:Constants.API_BASE_URL + "/tags?filter=" + JSON.stringify(filter)
    }).then(response => {
        callback(response.data);
    }).catch(err => {
        console.log(err);
        callback([])
    })
}

export const addTag = (tagValue,callback) => {
    axios({
        method:"POST",
        url:Constants.API_BASE_URL + "/tags",
        data:{
            tag:tagValue
        }
    }).then(response => {
        callback(response.data);
    }).catch(err => {
        console.log(err);
        if(err.response.status == 400){
            callback(null,true);
        } else {
            callback(null,false)
        }
    })
}