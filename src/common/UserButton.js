import React from "react";
import { getUser, getUserDetails } from "./LoginUtils";
import UserIcon from "./UserIcon";
import {logoutUserFromServer} from "../common/LoginUtils";
import LoginComponent from "../common/Login";

export default class UserButton extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            loginDetails:null,
            userDetails:null,
            showPopup:false,
            showLogin:false
        }
    }

    componentDidMount(){
        this.checkUserLoginStatus();    
    }

    checkUserLoginStatus(){
        getUser((ud)=>{
            this.setState({
                userDetails:ud,
                loginDetails:getUserDetails()
            })
        });
    }

    render(){
        return(
            <div className="udw">
                {
                    this.state.loginDetails == null ?
                    <button className="cta-button-normal" onClick={()=>{
                        this.setState({showLogin:true});
                    }}>Login</button>
                    :
                    <div className="user-popup">
                        <div className={"user-details" + (this.state.showPopup ? " active" : "")} onClick={()=>{
                            this.setState({
                                showPopup:!this.state.showPopup
                            })
                        }}>
                            <UserIcon className={"small" + (this.props.theme == "light" ? " light" : "")}/>
                            <span style={{color:(this.props.theme == "light" ? "#fff" : "#000")}}>{this.state.userDetails ? this.state.userDetails.username : ""}</span>
                        </div>
                        <div className={`user-popup-wrap` + (this.state.showPopup ? " show" : "")}>
                            <div className="user-popup-option">Profile</div>
                            <div className="user-popup-option" onClick={()=>{
                                this.setState({
                                    showPopup:false
                                });
                                if(confirm("Are you sure?")){
                                    logoutUserFromServer((err)=>{
                                        if(err){
                                            alert("Could not log you out, please try again");
                                        } else {
                                            this.checkUserLoginStatus();
                                            this.props.userLoggedOut();
                                            window.location.reload();
                                        }
                                    })
                                }
                            }}>Logout</div>
                        </div>
                    </div>
                }
                {
                    this.state.showLogin ? 
                    <LoginComponent 
                        onCloseButtonClicked={()=>{
                            this.setState({
                                showLogin:false
                            })
                        }} onLoginSuccess={()=>{
                            this.setState({
                                showLogin:false
                            },()=>{
                                this.checkUserLoginStatus();
                                this.props.userLoggedIn();
                            })
                        }}/>
                    :
                    null
                }
            </div>
        )
    }
}