import React from "react";
import {getUser} from "../common/LoginUtils";

export default class UserIcon extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            loginUser:null
        }
    }

    componentDidMount(){
        getUser((ud) => {
            this.setState({
                loginUser:ud
            })
        })
    }

    render(){
        let names = "U S".split(" ");
        if(this.state.loginUser != null){
            names = this.state.loginUser.username.split(" ");
        }
        return(
            <div className={`uicon ` + this.props.className}>
                {
                    names[0].substring(0,1)
                }
                {
                    names[1] ? 
                    <span>
                        {
                            names[1].substring(0,1)
                        }
                    </span>
                    :
                    names[0].substring(1,2)
                }
            </div>
        )
    }
}