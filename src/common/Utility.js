import moment from "moment";

export const escapeQuotes = (str) => {
    return str.replace(/"/g, '\\"');
}

export const unescapeQuotes = (str) => {
    return str.replace(/\\"/g, '"');
}

export const getFormattedDateForNote = (dt) => {
    return moment(dt).format("MMM DD, YYYY HH:mm A")
}