import React from "react";

const CustomToolbar = () => (
    <div id="toolbar">
        <select className="ql-header" defaultValue={""} onChange={e => e.persist()}>
            <option value="1" />
            <option value="2" />
            <option value="3" />
        </select>
        <button className="ql-bold" />
        <button className="ql-italic" />
        <button className="ql-underline" />
        <button className="ql-strike" />
        <button className="ql-blockquote" />
        <button className="ql-link" />
        <button className="ql-list" value="bullet"></button>
        <button className="ql-list" value="ordered"></button>
        <button className="ql-indent" value="-1"></button>
        <button className="ql-indent" value="+1"></button>
        <button className="ql-image">
            II
      </button>
      <button className="ql-clean"/>
    </div>
);

export default CustomToolbar;