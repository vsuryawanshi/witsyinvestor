export default {
    API_BASE_URL:"http://62.210.93.54:9800/witsy/api/v1",
    ENC:"witsycookie",
    OPTS : [
        {
            text:"Research Papers",
            image:require("../images/research-papers.svg"),
            color:"#ee7493",
            url:"/researchpapers"
        },
        {
            text:"Collections",
            image:require("../images/usecases.svg"),
            color:"#f2b3e7",
            url:"/collections"
        },
        {
            text:"Events",
            image:require("../images/events.svg"),
            color:"#d8adee"
        },
        {
            text:"Top Researchers",
            image:require("../images/top-researchers.svg"),
            color:"#9089f4"
        },
        {
            text:"GPU",
            image:require("../images/gpu.svg"),
            color:"#4298f1"
        }
    ],
    USEROPTS : [
        {
            text:"Account Settings",
            icon:require("../images/settings.svg")
        },
        {
            text:"Log out",
            icon:require("../images/logout.svg")
        }
    ],
    FILTERS : ["Most Popular", "Latest", "Relevant"],
    LOGIN_COOKIE:"loginCookie",
    USERDATA_COOKIE:"udc",
    USERPDATA:"upd",
    BG_COLORS:[
        "#ee7493",
        "#f2b3e7",
        "#d8adee",
        "#9089f4",
        "#4298f1"
    ],
    TAGCOLORS:[
        {
            bgcolor:"#dfe9f0",
            color:"#4f6f89"
        },
        {
            bgcolor:"#7fd4b0",
            color:"#2a8774"
        },
        {
            bgcolor:"#b1b5f2",
            color:"#5d65d9"
        },
        {
            bgcolor:"#f2d6b1",
            color:"#d88920"
        },
        {
            bgcolor:"#b1d9f2",
            color:"#1b81c0"
        },
        {
            bgcolor:"#9ab5db",
            color:"#3065ae"
        }
    ]
}