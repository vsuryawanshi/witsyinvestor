import React from "react";
import Particles from 'react-particles-js';
import {PARTICLES_PARAMS} from "../partials/home/hero-section";
import { Link, DirectLink, Element , Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

var durationFn = function(deltaTop) {
    return deltaTop;
};

export default class AmbassadorPage extends React.Component{
    constructor(props){
        super(props);
        this.currentScrollTop = 0;
    }

    componentDidMount(){
        document.body.addEventListener("mousewheel", this.scrollHandler.bind(this));
        document.body.addEventListener("DOMMouseScroll", this.scrollHandler.bind(this));
    }

    scrollHandler(){
        var scrollTop = this.container.scrollTop;
        if(scrollTop > this.currentScrollTop){
            //downscroll
            this.headerElem.classList.add("hide");
        } else {
            this.headerElem.classList.remove("hide");
        }
        this.currentScrollTop = scrollTop;
    }

    componentWillUnmount(){
        document.body.removeEventListener("mousewheel", this.scrollHandler);
        document.body.removeEventListener("DOMMouseScroll", this.scrollHandler);   
    }

    render(){
        return(
            <div className="ambassador-wrap" id="scont" ref={node => this.container = node}>
                <section className="ab-header" ref={node => this.headerElem = node}>
                    <div className="hcontainer">
                        <div className="logo"/>
                        <ul>
                            <li>
                                <Link className="nav-text" activeClass="active" to="one" spy={true} smooth={true} duration={500} containerId="scont" offset={-50}>About Witsy</Link>
                            </li>
                            <li>
                                <Link className="nav-text" activeClass="active" to="two" spy={true} smooth={true} duration={500} containerId="scont" offset={-50}>Objectives</Link>
                            </li>
                            <li>
                                <Link className="nav-text" activeClass="active" to="three" spy={true} smooth={true} duration={500} containerId="scont" offset={-50}>What are we looking for</Link>
                            </li>
                            <li>
                                <Link className="nav-text" activeClass="active" to="four" spy={true} smooth={true} duration={500} containerId="scont" offset={-50}>What do you get?</Link>
                            </li>
                            <li>
                                <Link className="nav-text" activeClass="active" to="five" spy={true} smooth={true} duration={500} containerId="scont" offset={-50}>Selection Procedure</Link>
                            </li>
                        </ul>
                    </div>
                </section>
                <section className="first-section">
                    <Particles params={PARTICLES_PARAMS} className="orb-wrap"/>
                    <div className="first-content">
                        <h1 className="htitle">Witsy Ambassador Program</h1>
                        <h3 className="hsubtitle">
                            Looking for Ambassadors from engineering colleges in:
                            <br/>
                            Delhi | UP | Madhya Pradesh | Bihar | Telangana
                        </h3>
                        <div className="pgraph2">
                            Guided semester-long program, wherein ambassadors create awareness about power of Kreatryx and our products on your campus and in turn accelerate your professional career 
                        </div>
                        <div className="btn-container">
                            <a className="cta-button-large join">Join WAP</a>
                        </div>
                    </div>
                </section>

                <Element className="about-section" name="one">
                    <div className="container">
                        <h3 className="heading-3">About Witsy</h3>
                        <div className="divider"><div/><div/></div>
                        <p className="paragraph">WAP is keenly focused on taking the Indian Education Industry and its members to newer heights.</p>
                        <div className="row items">
                            <div className="w25">
                                <div className="txt2">4m+</div>
                                <div className="txt9">Attempted Questions</div>
                            </div>
                            <div className="w25">
                                <div className="txt2">50k+</div>
                                <div className="txt9">Tests taken</div>
                            </div>
                            <div className="w25">
                                <div className="txt2">50k+</div>
                                <div className="txt9">Registrations</div>
                            </div>
                            <div className="w25">
                                <div className="txt2">142k+</div>
                                <div className="txt9">Members on Facebook group</div>
                            </div>
                        </div>
                    </div>
                </Element>

                <Element className="objectives"  name="two">
                    <div className="container">
                        <h3 className="heading-3">Objectives</h3>
                        <div className="divider"><div/><div/></div>
                        <p className="paragraph">
                            KAP is an important part of our Kreatryx team made up of the country’s top students leaders working together
                            <br/>
                            to help save other students time and money by leading a movement for affordable test series,
                            <br/>
                            courses and textbooks. 
                            <br/>
                            <br/>
                            The ambassadors will be our eyes, ears and voices at their college campuses
                        </p>
                        <div className="row">
                            <div className="w33">
                                <div className="card">
                                    <h4>Our Goal</h4>
                                    <p className="paragraph-3">
                                    The Campus Ambassador program at Kreatryx is aimed at identifying energetic, passionate, brilliant, like minded College Superheroes and Wonder Women who have the zeal to take initiative and can make a mark.
                                    </p>
                                </div>
                            </div>
                            <div className="w33">
                                <div className="card">
                                    <h4>We need Superheroes</h4>
                                    <p className="paragraph-3">
                                    We’re in search of one-two student leaders and campus influencers in each college to spread the word about Kreatryx products and activities.
                                    <br/>
                                    <br/>
                                    As a Campus Ambassador you’ll be representing all our products to students on campus and generating revenue for our company.
                                    </p>
                                </div>
                            </div>
                            <div className="w33">
                                <div className="card">
                                    <h4>How You Can Help</h4>
                                    <p className="paragraph-3">
                                    You will act as the liaison between us and your college. You will host events and network with students on campus to attract candidates for our brand awareness. 
                                    <br/>
                                    <br/>
                                    You have to attend our training program and participate in weekly status updates and monthly calls with Kreatryx team.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <br/>
                    </div>
                </Element>

                <Element className="eligibility" name="three">
                    <div className="container">
                        <h3 className="heading-3">What are we looking for?</h3>
                        <div className="divider"><div/><div/></div>
                        <p className="paragraph">
                            Well, they are the people who are typically great orators and very active members of college societies, clubs, events and activities. In addition, they are stars of their community and people look up to them since they usually lead by example. In short, Leadership flows in their veins and they are the visionaries of tomorrow.   
                            <br/>
                            We require someone who can inspire peers, collaborate with campus organizations, and get things done. 
                            <br/>
                            <br/>
                            Ambassadors should be:
                        </p>
                        <br/>
                        <br/>
                        <div className="row fs">
                            <div className="w33 bullet-col">
                                <img src={require("../images/ic_bullet.png")}/>
                                <p className="paragraph-4">Devoting 8-10 hours a week to the program</p>
                            </div>
                            <div className="w33 bullet-col">
                                <img src={require("../images/ic_bullet.png")}/>
                                <p className="paragraph-4">Excellent inter-personal and communication skills</p>
                            </div>
                            <div className="w33 bullet-col">
                                <img src={require("../images/ic_bullet.png")}/>
                                <p className="paragraph-4">Organised, ambitious, reliable, out of the box & creative thinker</p>
                            </div>
                        </div>
                        <br/>
                        <div className="row fs">
                            <div className="w33 bullet-col">
                                <img src={require("../images/ic_bullet.png")}/>
                                <p className="paragraph-4">Interested in Sales/Marketing/ Entrepreneurship</p>
                            </div>
                            <div className="w33 bullet-col">
                                <img src={require("../images/ic_bullet.png")}/>
                                <p className="paragraph-4">Involved in student organisations, teams, and/or clubs</p>
                            </div>
                            <div className="w33 bullet-col">
                                <img src={require("../images/ic_bullet.png")}/>
                                <p className="paragraph-4">Able to commit for the entire duration of the program (August - December)</p>
                            </div>
                        </div>
                        <br/>
                        <div className="row fs">
                            <div className="w33 bullet-col">
                                <img src={require("../images/ic_bullet.png")}/>
                                <p className="paragraph-4">Pursuing 2nd/3rd Year</p>
                            </div>
                            <div className="w33 bullet-col">
                                <img src={require("../images/ic_bullet.png")}/>
                                <p className="paragraph-4">Able to demonstrate leadership skills/past voluntary experiences/interest in promotion activities.</p>
                            </div>
                        </div>
                    </div>
                </Element>

                <Element className="incentives" name="four">
                    <div className="container">
                        <h3 className="heading-5">What do you get</h3>
                        <div className="divider white"><div/><div/></div>

                        <div className="row">
                            <div className="w50 e-col">
                                <img src={require("../images/ic_certificate.png")}/>
                                <h4 className="heading-12">Certificates</h4>
                                <p className="paragraph-5">
                                    Engrave your experience with
                                    <br/>
                                    certificates of appreciation from Kreatryx
                                </p>
                            </div>
                            <div className="w50 e-col">
                                <img src={require("../images/ic_seminars.png")}/>
                                <h4 className="heading-12">Free Guidance</h4>
                                <p className="paragraph-5">
                                    Get updates and learn the relevant
                                    <br/>
                                    skills to grow your professional career
                                    <br/>
                                    under the guidance of Team Kreatryx
                                </p>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <br/>
                        <div className="row">
                            <div className="w50 e-col">
                                <img src={require("../images/ic_mugs.png")}/>
                                <h4 className="heading-12">Free Merchandise</h4>
                                <p className="paragraph-5">
                                    Take memories home with goodies
                                    <br/>
                                    given free of cost
                                </p>
                            </div>
                            <div className="w50 e-col">
                                <img src={require("../images/ic_recommendations.png")}/>
                                <h4 className="heading-12">Recommendations/Recognition</h4>
                                <p className="paragraph-5">
                                    Letter of recommendation and LinkedIn
                                    <br/>
                                    recommendations for the best
                                    <br/>
                                    campus ambassadors
                                </p>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <br/>
                    </div>
                </Element>
                <Element className="selection" name="five">
                    <div className="container">
                        <h3 className="heading-3">Selection Procedure</h3>
                        <div className="divider"><div/><div/></div>

                        <br/>
                        <br/>
                        <div className="row">
                            <div className="w33">
                                <h1 className="heading13">1</h1>
                                <p className="paragraph6">Application Form</p>
                            </div>
                            <div className="w33">
                                <h1 className="heading13">2</h1>
                                <p className="paragraph6">Telephonic or<br/>Skype Interview</p>
                            </div>
                            <div className="w33">
                                <h1 className="heading13">3</h1>
                                <p className="paragraph6">Task based performance</p>
                            </div>
                        </div>
                        
                        <br/><br/>
                    </div>
                </Element>

                <section className="application">
                    <div className="container">
                        <h3 className="heading-3">Application</h3>
                        <div className="divider"><div/><div/></div>
                        <br/>
                        <br/>  
                        <p className="paragraph7">
                            The last date to apply is 15th August, 2018
                            <br/>
                            Kindly spare some time to fill this form.
                            <br/>
                            Give as much detailed information about yourself as you can give. 
                            <br/>
                        </p>  
                        <a className="blue-button">Join WAP</a>
                    </div>
                </section>

                <section className="selection-footer">
                    <div className="container">
                        <div className="tb-12">
                            For any queries, you can drop a mail @ varun@deeppulse.ai or you can call us at +91-111111111
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}