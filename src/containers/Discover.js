import React from "react";
import Constants from "../common/constants";
import {getUser,getUserDetails} from "../common/LoginUtils";
import UserButton from "../common/UserButton";
import axios from "axios";

export default class Discover extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            apiCallInProgress:false,
            selectedOption:-1,
            selectedFilterIndex:-1,
            isUserLoggedIn:false,
            showLogin:false,
            items:[],
            loginUser:null,
            userDetails:null,
            showAddCollection:false,
            collectionName:""
        }
    }

    componentDidMount(){
        this.setState({
            selectedOption:0
        },()=>{
            if(Constants.OPTS[0].url){
                this.getCategoryData(Constants.OPTS[0].url);
            }
        });
        this.checkUserLoginStatus();
    }

    checkUserLoginStatus(){
        getUser((ud)=>{
            this.setState({
                loginUser:JSON.parse(getUserDetails()),
                userDetails:ud
            })
        })
    }

    getCategoryData(catUrl){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios.get(Constants.API_BASE_URL + catUrl).then(response => {
                this.setState({
                    items:response.data,
                    apiCallInProgress:false
                })
            }).catch(err => {
                console.log(err.response);
                this.setState({
                    apiCallInProgress:false
                })
            })
        })
    }

    handleClick(item){
        switch(this.state.selectedOption){
            case 0:
                this.props.history.push("/pdf/" + item.id);
                break;
            case 1:
                this.props.history.push("/notes/" + item.id + "/" + encodeURIComponent(item.title));
                break;
        }
    }

    addCollection(){
        if(this.state.loginUser !== null){
            this.setState({
                apiCallInProgress:true
            },()=>{
                axios({
                    method:"POST",
                    url:Constants.API_BASE_URL + "/collections",
                    data:{
                        "docType": "collection",
                        "title": this.state.collectionName,
                        "createdBy": this.state.userDetails.userId
                    }
                }).then(response => {
                    this.setState({
                        showAddCollection:false,
                        apiCallInProgress:false
                    },()=>{
                        this.getCategoryData(Constants.OPTS[this.state.selectedOption].url);
                    })
                }).catch(err => {
                    this.setState({
                        apiCallInProgress:false
                    })
                    alert("Could not add collection, please try again");
                })
            })
        }
    }

    render(){
        return(
            <div className="discover-wrap">
                {
                    this.state.apiCallInProgress ? <div className="progress"><div className="indeterminate"></div></div> : null
                }
                
                <aside className="sidebar">
                    <div className="inner">
                        <div className="top">
                            <img src={require("../images/logo-blue.svg")} className="logo"/>
                        </div>
                        <div className="options">
                            {
                                Constants.OPTS.map((op,i)=>{
                                    return(
                                        <div 
                                            className={`option` + (this.state.selectedOption == i ? " selected" : "")} 
                                            key={i} 
                                            onClick={()=>{
                                                this.setState({
                                                    selectedOption:i
                                                },()=>{
                                                    if(op.url){
                                                        this.getCategoryData(op.url);
                                                    }
                                                });
                                            }}>
                                            <div className="dot" style={{backgroundColor:op.color}}/>
                                            <img src={op.image}/>
                                            <div className="txt" style={{color:op.color}}>{op.text}</div>
                                            <div className={"vline"} style={{backgroundColor:op.color}}/>
                                        </div>
                                    );
                                })
                            }
                        </div>
                    </div>
                </aside>

                <div className="content-section">
                    <div className="header-s">
                        <h1 className="main-t">Discover</h1>
                        <div className="search-wrap">
                            <input className="inps" ref={node => this.searchInput = node} placeholder="Search for something..."/>
                        </div>
                        <UserButton
                            userLoggedOut={()=>{
                                this.checkUserLoginStatus();
                            }}
                            userLoggedIn={()=>{
                                this.checkUserLoginStatus();
                            }}/>
                    </div>
                    <div className="content">
                        <div className="filters" style={{display:"none"}}>
                            <div className="lbl">Filter by:</div>
                            {
                                Constants.FILTERS.map((fl,i)=>{
                                    return(
                                        <div className={"filter" + (this.state.selectedFilterIndex == i ? " active" : "")} key={i} onClick={()=>{
                                            this.setState({
                                                selectedFilterIndex:i
                                            });
                                        }}>{fl}</div>
                                    )
                                })
                            }
                        </div>
                        {
                            this.state.items.length > 0 ?
                            <span style={{opacity:(this.state.apiCallInProgress ? 0.5 : 1)}}>
                                {
                                    this.state.items.map((item,index)=>{
                                        return(
                                            <div className="card-wrap" key={index} onClick={()=>{
                                                this.handleClick(item);
                                            }}>
                                                <div className="card">
                                                    <div className="pic" style={{backgroundColor:Constants.BG_COLORS[index%5]}}>
                                                        <div className="fadetext">{item.title.substring(0,10)}</div>    
                                                    </div>
                                                    <div className="ctitle">{item.title.length > 50 ? item.title.substring(0,50) + "..." : item.title}</div>
                                                    {
                                                        item.description ?
                                                        <div className="cdesc">{item.description.length > 100 ? item.description.substring(0,100) + "..." : item.description}</div>
                                                        :
                                                        null
                                                    }
                                                    {
                                                        item.authors && item.authors.length > 0 ?
                                                        <div className="cdesc">
                                                            {
                                                                item.authors.map((auth,idx) => {
                                                                    return(
                                                                        <span key={idx} style={{color:Constants.BG_COLORS[idx%5]}}>{auth.name + " "}</span> 
                                                                    )
                                                                })
                                                            
                                                            
                                                            }
                                                        </div>
                                                        :
                                                        null
                                                    }
                                                    <div className="card-footer">
                                                        <div className="mlogo even">M</div>
                                                        <div className="actions">
                                                            <img src={require("../images/forward.svg")} className="ctaimg"/>
                                                            <img src={require("../images/bookmark-white.svg")} className="ctaimg"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                        )
                                    })
                                }
                            </span>
                            :
                            <div className="no-results">
                                No Results to show
                                {
                                    this.state.selectedOption == 1 ?
                                    <div className="add-wrap">
                                        {
                                            this.state.loginUser != null ?

                                            <div className="add-action">
                                                <button className="btnl" onClick={()=>{
                                                    this.setState({
                                                        showAddCollection:true
                                                    })
                                                }}>Add Collection</button>
                                            </div>
                                            :
                                            <div className="add-action">
                                                Please login to add a new collection
                                            </div>
                                        }
                                    </div>
                                    :
                                    null
                                }
                            </div>
                        }
                    </div>
                </div>
                {
                    this.state.showAddCollection ? 
                    <div className="modal-backdrop">
                        <div className="modal">
                            <div className="close-btn"/>
                            <div className="mtitle">Add a new collection</div>
                            <div className="mcontent">
                                <div className="form-elem">
                                    <label htmlFor="cn">Collection Name</label>
                                    <input placeholder="Collection Name" name="cn" type="text" value={this.state.collectionName} onChange={(e)=>{
                                        this.setState({
                                            collectionName:e.target.value
                                        });
                                    }}/>
                                </div>
                            </div>
                            <div className="mactions">
                                <button type="button" disabled={this.state.collectionName == ""} className="btnl" onClick={()=>{
                                    this.addCollection();
                                }}>Add</button>
                            </div>
                        </div>
                    </div>
                    :
                    null
                }
            </div>
        )
    }
}