import React, { Component } from "react";
import Header from "../common/Header";

import HeroSection from "../partials/home/hero-section";
import FindingSection from "../partials/home/finding-section";
import WhyAtWorkSection from "../partials/home/why-at-work-section";
import CompanyInformation from "../partials/home/company-information";
import OvercomeSection from "../partials/home/overcome";
import MeetWitsySection from "../partials/home/meet-witsy";
import InfoCapturingSection from "../partials/home/info-capturing";
import HowWitsyWorksSection from "../partials/home/how-witsy-works";
import InstantPainlessSection from "../partials/home/instant-painless";
import FooterSection from "../partials/home/footer-section";

export default class Home extends Component {
    constructor(props){
        super(props);
    }
    

    render() {
        return (
            <div className="main">
                <Header />
                <HeroSection/>
                <img src={require("../images/parallelogram.png")} className="alpha-shape"></img>
                <FindingSection/>
                <WhyAtWorkSection/>
                <CompanyInformation/>
                <OvercomeSection/>
                <MeetWitsySection/>
                <InfoCapturingSection/>
                <HowWitsyWorksSection/>
                <InstantPainlessSection/>
                <FooterSection/>
            </div>
        );
    }
}