import React from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

// fake data generator
const getItems = (count, offset = 0) =>
    Array.from({ length: count }, (v, k) => k).map(k => ({
        id: `item-${Math.random().toString()}`,
        content: `This is task ${k + offset}`
    }));

const LANES = [
    {
        id:"lane-todo",
        name:"To-do",
        items:getItems(5),
        color:"#7f7efe"
    },
    {
        id:"lane-inprogress",
        name:"In Progress",
        items:getItems(9),
        color:"#f9b85c"
    },
    {
        id:"lane-done",
        name:"Done",
        items:getItems(8),
        color:"#f786d6"
    }
]

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = Array.from(source);
    const destClone = Array.from(destination);
    const [removed] = sourceClone.splice(droppableSource.index, 1);

    destClone.splice(droppableDestination.index, 0, removed);

    const result = {};
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;

    return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    margin: `0 0 ${grid}px 0`,
    // change background colour if dragging
    background: isDragging ? '#eee' : '#fff',

    // styles we need to apply on draggables
    ...draggableStyle
});

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? 'rgba(0,0,0,0)' : 'rgba(0,0,0,0)',
    width:292,
    display:"inline-block",
    verticalAlign:"top"
});

export default class LearningBoard extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            boardData:JSON.parse(JSON.stringify(LANES)),
            items: getItems(10),
            selected: getItems(5, 10)
        };
    }

    getList(id){
        let arrToReturn = [];
        this.state.boardData.map((bd)=>{
            if(bd.id == id){
                arrToReturn = bd.items;
                return;
            }
        })
        return arrToReturn;
    }

    onDragEnd(result) {
        const { source, destination } = result;

        // dropped outside the list
        if (!destination) {
            return;
        }
        if (source.droppableId === destination.droppableId) {
            console.log("droppped in same");
            const items = reorder(
                this.getList(source.droppableId),
                source.index,
                destination.index
            );

            // if (source.droppableId === 'droppable2') {
            //     state = { selected: items };
            // }

            var stateCopy = this.state.boardData;
            stateCopy.map((bd)=>{
                if(bd.id == source.droppableId){
                    bd.items = items;
                }
            });
            this.setState({
                boardData : stateCopy
            });
        } else {
            console.log("droppped in DIFFERENT");
            const result = move(
                this.getList(source.droppableId),
                this.getList(destination.droppableId),
                source,
                destination
            );

            var stateCopy = this.state.boardData;
            stateCopy.map((bd)=>{
                if(bd.id == source.droppableId){
                    bd.items = result[source.droppableId]
                }
                if(bd.id == destination.droppableId){
                    bd.items = result[destination.droppableId]
                }
            });
            this.setState({
                boardData:stateCopy
            });
        }
    }

    render(){
        return(
            <div className="lboard-wrap">
                <div id="surface">
                    <div id="header">
                        <div className="header-container">
                            <div className="logo"/>
                            <div className="user">
                                <img src={require("../images/nancy_hopkins.jpg")}/>
                                <div className="name">Nancy Hopkins</div>
                            </div>
                        </div>
                    </div>  
                    <div id="content">
                        <DragDropContext onDragEnd={this.onDragEnd.bind(this)}>
                            <div className="board-wrapper">
                                <div className="board-main-content">
                                    <div className="board-header">
                                        <h1 className="board-title">Your learning board</h1>
                                    </div>

                                    <div className="board-canvas">
                                        <div id="board" className="u-fancy-scrollbar">
                                            {
                                                this.state.boardData.map((board,idx)=>{
                                                    return(
                                                        <div className="list-wrapper" key={board.id}>
                                                            <div className="list">
                                                                <div className="list-header">
                                                                    <h2 className="list-header-name">{board.name}</h2>
                                                                </div>
                                                                <Droppable droppableId={board.id}>
                                                                    {(provided, snapshot) => (
                                                                        <div
                                                                            className="list-cards"
                                                                            ref={provided.innerRef}
                                                                            style={getListStyle(snapshot.isDraggingOver)}>
                                                                            {board.items.map((item, index) => (
                                                                                <Draggable
                                                                                    key={item.id}
                                                                                    draggableId={item.id}
                                                                                    index={index}>
                                                                                    {(provided, snapshot) => (
                                                                                        <div
                                                                                            ref={provided.innerRef}
                                                                                            {...provided.draggableProps}
                                                                                            {...provided.dragHandleProps}
                                                                                            className="list-item"
                                                                                            style={getItemStyle(
                                                                                                snapshot.isDragging,
                                                                                                provided.draggableProps.style
                                                                                            )}>
                                                                                            <div className="strip" style={{backgroundColor:board.color}}/>
                                                                                            <div className="card-t">{item.content}</div>
                                                                                            <div className="date">
                                                                                                September 15
                                                                                            </div>
                                                                                            <div className="three-dots"/>
                                                                                        </div>
                                                                                    )}
                                                                                </Draggable>
                                                                            ))}
                                                                            {provided.placeholder}
                                                                        </div>
                                                                    )}
                                                                </Droppable>
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                            }  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </DragDropContext>
                    </div>             
                </div>
            </div>
        )  
    }
}