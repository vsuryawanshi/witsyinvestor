import React, {Component} from "react";
import { Switch, Route } from "react-router-dom";

import Home from "./Home";
import Notfound from "./NotFound";
import Discover from "./Discover";
import LearningBoard from "./LearningBoard";
import AmbassadorPage from "./Ambassador";
import PdfView from "./PdfView";
import Notes from "./Notes";
import VerifyPage from "./VerifyPage";

export default class NavRoot extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <main>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/discover" component={Discover}/>
                    <Route exact path="/learning" component={LearningBoard}/>
                    <Route exact path="/ambassador" component={AmbassadorPage}/>
                    <Route exact path="/pdf/:pdfid" component={PdfView}/>
                    <Route exact path="/notes/:noteid/:notename" component={Notes}/>
                    <Route exact path="/verify" component={VerifyPage}/>
                    <Route path="*" component={Notfound} />
                </Switch>
            </main>
        );
    }
}