import React, {Component} from "react";

export default class NotFound extends Component {
    render(){
        return(
            <div className="not-found">
                <h1>The page you are looking for does not exist here</h1>
            </div>
        );
    }
}