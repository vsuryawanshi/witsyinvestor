import React from "react";
import AddNote from "../partials/notes/AddNote";
import {getUserDetails, getUser} from "../common/LoginUtils";
import axios from "axios";
import Constants from "../common/constants";
import Parser from 'html-react-parser';
import {unescapeQuotes} from "../common/Utility";
import UserIcon from "../common/UserIcon";
import UserButton from "../common/UserButton";

export default class Notes extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            apiCallInProgress:false,
            showAddNote : false,
            loginUser : null,
            notes:[],
            userDetails:null,
            currentNote:null
        }
    }

    componentDidMount(){
        this.checkUserLoginStatus();
        this.getNotes();
    }

    checkUserLoginStatus(){
        getUser((ud)=>{
            this.setState({
                loginUser:JSON.parse(getUserDetails()),
                userDetails:ud
            })
        })
    }

    getNotes(){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios({
                method:"GET",
                url:Constants.API_BASE_URL + "/collections/" + this.props.match.params.noteid + "/stickynotes"
            }).then(response => {
                console.log(response.data);
                this.setState({
                    apiCallInProgress:false,
                    notes:response.data
                })
            }).catch(err => {
                this.setState({
                    apiCallInProgress:false
                })
                console.log(err);
            })
        })
    }

    deleteNote(noteId){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios({
                method:"DELETE",
                url:Constants.API_BASE_URL + "/collections/" + this.props.match.params.noteid + "/stickynotes/" +noteId 
            }).then(response => {
                this.getNotes();
            }).catch(err => {
                this.setState({
                    apiCallInProgress:false
                })
                console.log(err);
            })
        })    
    }

    render(){
        return(
            <div className="notes-wrap">
                {
                    this.state.apiCallInProgress ? <div className="progress"><div className="indeterminate"></div></div> : null
                }
                {
                    this.state.showAddNote ? 
                    <div className="add-note-backdrop">
                        <AddNote onCloseClicked={()=>{
                            this.setState({
                                showAddNote:false
                            })
                        }} noteAdded={()=>{
                            this.setState({
                                showAddNote:false
                            },()=>{
                                this.getNotes();
                            })
                        }}
                        noteUpdated={()=>{
                            this.setState({
                                showAddNote:false
                            },()=>{
                                this.getNotes();
                            })
                        }}
                        readOnly={this.state.loginUser == null}
                        noteData={this.state.currentNote} 
                        collectionId={this.props.match.params.noteid}
                        userId={this.state.loginUser ? this.state.loginUser.userId : null}
                        at={this.state.loginUser ? this.state.loginUser.id : null}/> 
                    </div>
                    :
                    null
                }
                <div className="notes-header">
                    <h1 className="main-t">Notes</h1>
                    <div className="search-wrap">
                        <input className="inps" ref={node => this.searchInput = node} placeholder="Search for something..."/>
                    </div>
                    <div className="user">
                        <UserButton
                            userLoggedOut={()=>{
                                this.checkUserLoginStatus();
                            }}
                            userLoggedIn={()=>{
                                this.checkUserLoginStatus();
                            }}/>
                    </div>
                </div>
                <div className="notes-content">
                    <div className="container">
                        <div className="note-section">
                            <div className="section-header">
                                <img src={require("../images/folder.svg")}/>
                                <span className="section-title">{this.props.match.params.notename}</span>
                                <span className="count">({this.state.notes.length})</span>
                                <div className="hline"/>
                                <img src={require("../images/dustbin.svg")}/>
                                <img src={require("../images/settings.svg")}/>
                            </div>
                            <div className="section-notes">
                                <div className="note-add-card " onClick={()=>{
                                    if(this.state.loginUser !== null){
                                        this.setState({
                                            currentNote:null,
                                            showAddNote:true
                                        })
                                    } else {
                                        alert("You need to login first");
                                    }
                                }}>
                                    <div className="add-circle">
                                        <img src={require("../images/add.svg")}/>
                                    </div>
                                    <div className="add-header">Add New</div>
                                    <div className="add-desc">Create a new note in your collection where you can add details</div>
                                </div>
                                {
                                    this.state.notes.map((nt,idx)=>{
                                        return(
                                            <div className="note-card" key={idx} onClick={()=>{
                                                this.setState({
                                                    currentNote:nt,
                                                    showAddNote:true
                                                })
                                            }}>
                                                <div className="u">
                                                    <div className="note-title">{nt.title}</div>
                                                    <div className="note-desc">{Parser(unescapeQuotes(nt.body))}</div>
                                                </div>
                                                <div className="b">
                                                    <div className="tags">
                                                        {
                                                            nt.tags.map((tg,idx)=>{
                                                                return(
                                                                    <div className="note-tag" key={idx}>{tg.tag}</div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                    <img src={require("../images/recycle-bin.svg")} className="pencil" onClick={(e)=>{
                                                        e.preventDefault();
                                                        e.stopPropagation();
                                                        if(confirm("Are you sure?")){
                                                            //this.deleteNote(nt.id);
                                                        }
                                                    }}/>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}