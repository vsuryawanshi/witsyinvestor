import React from "react";
import { Document, Page } from "react-pdf";
import axios from "axios";
import Constants from "../common/constants";
import {getUserDetails,getUser} from "../common/LoginUtils";
import UserButton from "../common/UserButton";

export default class PdfView extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            apiCallInProgress:false,
            numPages:0,
            pageNumber:1,
            pdfData:null,
            pdfContainerWidth:200,
            popupTop:0,
            popupLeft:0,
            showPopup:false,
            readPopupLeft:0,
            readPopupTop:0,
            showReadPopup:false,
            currentAnnotations:[],
            selectedAnnotation:null,
            selectedTab:0,
            tempPageNumber:0,
            loginUser:null,
            userDetails:null
        }

        this.tempSelection = null;
    }

    componentDidMount(){
        this.checkUserLoginStatus();
        this.getPdfDetails();
    }

    checkUserLoginStatus(){
        getUser((ud)=>{
            this.setState({
                loginUser:JSON.parse(getUserDetails()),
                userDetails:ud
            })
        })
    }

    getPdfDetails(){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios({
                method:"GET",
                url:Constants.API_BASE_URL + "/researchpapers/" + this.props.match.params.pdfid
            }).then(response => {
                this.setState({
                    pdfData:response.data,
                    apiCallInProgress:false
                },()=>{
                    if(this.state.loginUser !== null){
                        this.getPdfNotes();
                    }
                })
            }).catch(err => {
                console.log(err);
                this.setState({
                    apiCallInProgress:false
                })
            })
        })
    }

    getPdfNotes(){
        axios({
            method:"GET",
            url:Constants.API_BASE_URL + "/researchpapers/" + this.state.pdfData.id + "/notes",
            headers:{
                Authorization : this.state.loginUser.id
            }
        }).then(response => {
            this.setState({
                currentAnnotations:response.data
            });
        }).catch(err => {
            console.log(err);
        })
    }

    savePdfNote(note){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios({
                method:"POST",
                url:Constants.API_BASE_URL + "/researchpapers/" + this.state.pdfData.id + "/notes?access_token="+this.state.loginUser.id,
                data:{
                    "title": note.title,
                    "docType": "note",
                    "createdBy": this.state.loginUser.userId,
                    "notesDoc": {
                        "id":this.state.pdfData.id,
                        "docType":"resaerchPaper"
                    },
                    "pageNumber": this.state.pageNumber,
                    "coordinates": [
                      note.rect.left, note.rect.top
                    ],
                    "public": true,
                    "researchpaperId": this.state.pdfData.id
                }
            }).then(response => {
                console.log(response.data);
                this.setState({
                    currentAnnotations : [...this.state.currentAnnotations, response.data]
                })
            }).catch(err => {
                console.log(err);
            })
        })
    }

    onDocumentLoadSuccess(obj){
        this.pdfPageCount.value =1;
        this.pdfNav.style.display = "flex";
        this.setState({ 
            numPages:obj.numPages,
            pdfContainerWidth:this.pdfContainer.getBoundingClientRect().width,
            tempPageNumber:1,
            pageNumber:1
        });
    }

    getSelected() {
        if(window.getSelection) { return window.getSelection(); }
        else if(document.getSelection) { return document.getSelection(); }
        else {
            var selection = document.selection && document.selection.createRange();
            if(selection.text) { return selection.text; }
                return false;
            }
        return false;
    }

    restoreSelection(range) {
        if (range) {
            if (window.getSelection) {
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (document.selection && range.select) {
                range.select();
            }
        }
    }

    saveSelection() {
        if (window.getSelection) {
            var sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                return sel.getRangeAt(0);
            }
        } else if (document.selection && document.selection.createRange) {
            return document.selection.createRange();
        }
        return null;
    }

    clearAnnotationBox(){
        this.annotationDescription.value = "";
    }

    saveAnnotation(){
        if(this.state.loginUser !== null){
            console.log(this.state.loginUser);
            if(this.annotationDescription.value !== ""){
                var annObject = {};
                annObject.id = (new Date().getTime());
                annObject.title = this.annotationDescription.value;
                annObject.selection = this.tempSelection;
                annObject.rect = {
                    left:this.startX - 10,
                    top:this.startY - 40
                }
                this.clearAnnotationBox();
                this.setState({
                    showPopup:false
                },()=>{
                    this.savePdfNote(annObject);
                });
            }
        } else {
            alert("You need to login first");
            this.setState({
                showPopup:false
            });
        }
    }

    render(){
        return(
            <div className="pdf-view-wrap"> 
                <nav className="main-nav">
                    <div className="back-button" onClick={()=>{
                        if(this.props.history.length < 3){
                            console.log("pushing");
                            this.props.history.push("/discover");
                        } else {
                            console.log("back");
                            this.props.history.goBack();
                        }
                    }}/>
                    <div className="pdf-name">
                        <img src={require("../images/pdf.svg")}/>
                        <div className="pdf-name-details">
                            <div className="top">AI > Machine Learning</div>
                            <div className="bottom">{this.state.pdfData ? this.state.pdfData.arxivId + ".pdf" : "Loading..."}</div>
                        </div>
                    </div>
                    <div className="pdf-actions">
                        <button className="action-btn">
                            <img src={require("../images/edit.svg")}/>
                            Edit
                        </button>
                        <button className="action-btn">
                            <img src={require("../images/list.svg")}/>
                            Compare
                        </button>
                        <UserButton
                            userLoggedOut={()=>{
                                this.checkUserLoginStatus();
                            }}
                            userLoggedIn={()=>{
                                this.checkUserLoginStatus();
                            }}/>
                    </div>
                </nav>
                {
                    this.state.pdfData != null ?
                    <aside className="pdf-overview">
                        <div className="pdf-tabs">
                            <div className={"pdf-tab" + (this.state.selectedTab == 0 ? " active" : "")} onClick={()=>{
                                this.setState({
                                    selectedTab:0
                                });
                            }}>Details</div>
                            <div className={"pdf-tab" + (this.state.selectedTab == 1 ? " active" : "")} onClick={()=>{
                                this.setState({
                                    selectedTab:1
                                });
                            }}>Annotations ({this.state.currentAnnotations.length})</div>
                        </div>
                        {
                            this.state.selectedTab == 0 ?
                            <div className="pdf-tab-content">
                                <div className="pdfrow">
                                    <div className="rowlbl">Filename</div>
                                    <div className="rowval">{this.state.pdfData ? this.state.pdfData.pdf: "Loading..."}</div>
                                </div>
                                <div className="pdfrow">
                                    <div className="rowlbl">Authors</div>
                                    {
                                        this.state.pdfData.authors.map((auth,idx)=>{
                                            return(
                                                <div className="rowval" key={idx}>{auth.name}</div>

                                            )
                                        })
                                    }
                                </div>
                                <div className="pdfrow">
                                    <div className="rowlbl">TAG</div>
                                    <div className="tags">
                                        {
                                            this.state.pdfData.tags.map((ft,idx)=>{
                                                return(
                                                    <div className="tg" key={idx} style={{backgroundColor:Constants.TAGCOLORS[idx%6].bgcolor,color:Constants.TAGCOLORS[idx%6].color}}>{ft.tag}</div>
                                                );
                                            })
                                        }
                                    </div>
                                </div>
                                <div className="pdfrow">
                                    <div className="rowlbl">Description</div>
                                    <div className="rowval">{this.state.pdfData.description}</div>
                                </div>
                            </div>
                            :
                            <div className="pdf-tab-content">
                                {
                                    this.state.currentAnnotations.map((ann,idx)=>{
                                        return(
                                            <div className="ann" key={idx}>
                                                <div className="ann-title">{"Page " + ann.pageNumber}</div>
                                                <div className="ann-desc">{ann.title}</div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        }
                    </aside> 
                    :
                    null
                }  
                <main className="pcontent" ref={node => this.scrollingContainer = node}>
                {
                    this.state.pdfData != null ?
                    <div className="pcontainer" ref={node => this.pdfContainer = node}>
                        <Document
                            file={this.state.pdfData.pdf}
                            onLoadSuccess={this.onDocumentLoadSuccess.bind(this)}>
                                <Page
                                    onMouseDown={(e)=>{
                                        var rect = this.pdfContainer.getBoundingClientRect();
                                        this.startX = e.clientX - rect.left;;
                                        this.startY = e.clientY - rect.top;
                                    }}
                                    onMouseUp={(e)=>{
                                        var selection = this.getSelected();
                                        if(selection != ""){
                                            this.tempRect = selection.getRangeAt(0).getBoundingClientRect();
                                            this.tempSelection = this.saveSelection();

                                            //position the popup so that it doesnt get out of viewbounds
                                            let pl = e.clientX, pt = e.clientY;
                                            if(this.savePopup.clientWidth + e.clientX >= window.innerWidth){
                                                pl -= ((this.savePopup.clientWidth + e.clientX) - window.innerWidth + 20);
                                            }
                                            if(this.savePopup.clientHeight + e.clientY >= window.innerHeight){
                                                pt -= ((this.savePopup.clientHeight + e.clientY) - window.innerHeight + 20);
                                            }
                                            this.setState({
                                                popupLeft:pl,
                                                popupTop:pt,
                                                showPopup:true,
                                            })    
                                        }else{
                                            this.setState({
                                                showPopup:false
                                            },()=>{
                                                this.clearAnnotationBox();
                                            })
                                        }
                                    }}
                                    className="pdf-doc"
                                    pageNumber={this.state.pageNumber} 
                                    renderMode="canvas"
                                    width={this.state.pdfContainerWidth} >
                                    <div className="overlay">
                                        {
                                            this.state.currentAnnotations.map((ann,idx)=>{
                                                if(this.state.pageNumber == ann.pageNumber){
                                                    var _left = parseInt(ann.coordinates[0]);
                                                    var _top = parseInt(ann.coordinates[1]);
                                                    return(
                                                        <div className="highlight" key={idx} style={{
                                                            left:_left,
                                                            top:_top
                                                        }} onClick={()=>{
                                                            this.setState({
                                                                selectedAnnotation:ann,
                                                                showReadPopup:true,
                                                                readPopupLeft:_left + 40,
                                                                readPopupTop:_top + 40
                                                            });
                                                        }}/>
                                                    )
                                                }
                                            })
                                        }
                                    </div>    
                                </Page>
                        </Document>
                        <div 
                            className={"popup" + (this.state.showPopup ? " show" : "")} 
                            style={{
                                top:this.state.popupTop,
                                left:this.state.popupLeft
                            }} ref={node => this.savePopup = node}>
                            <div className="close-btn" onClick={()=>{
                                this.clearAnnotationBox();
                                this.setState({
                                    showPopup:false
                                });
                            }}/>
                            <div className="ptop">
                                <img src={require("../images/nancy_hopkins.jpg")}/>
                                <div className="tinfo">
                                    <div className="top">Nancy Hopkins</div>
                                    <div className="bottom">Carbon Based Lifeforms</div>
                                </div>
                            </div>
                            <div className="pform">
                                <div className="form-group">
                                    <textarea rows={3} placeholder="Please enter your note here..." ref={node => this.annotationDescription = node}/>
                                </div>
                            </div>
                            <div className="btn-wrap">
                                <button className="popup-btn" onClick={()=>{
                                    this.saveAnnotation();
                                }}>Add a note</button>
                            </div>
                        </div>

                        <div
                            className={"popup read" + (this.state.showReadPopup ? " show" : "")}
                            style={{
                                top:this.state.readPopupTop,
                                left:this.state.readPopupLeft
                            }} ref={node => this.readPopup = node}>
                            <div className="ptop">
                                <div className="pbg">VS</div>
                                <div className="tinfo">
                                    <div className="top">vsvanshi</div>
                                    <div className="bottom"></div>
                                </div>
                            </div>
                            <div className="pform">
                                <div className="form-group">{this.state.selectedAnnotation ? this.state.selectedAnnotation.title : ""}</div>
                            </div>
                            <div style={{display:"flex",justifyContent:"flex-end"}}>
                                <button className="save-btn btn" onClick={()=>{
                                    this.setState({
                                        showReadPopup:false
                                    })
                                }}>Ok</button>
                            </div> 
                        </div>
                    </div>
                    :
                    null
                }
                </main> 
                {
                    this.state.pdfData != null ?
                    <div className="pdf-navigation" style={{width:(this.pdfContainer && this.pdfContainer.clientWidth), display:"none"}} ref={node => this.pdfNav = node}>
                        <div className="nav-area">
                            <div className="pdf-page-nav">
                                <label>Page</label>
                                <input ref={node => this.pdfPageCount = node} type="text" onKeyDown={(e)=>{
                                    if(e.which == 13){
                                        this.setState({
                                            pageNumber:parseInt(e.target.value)
                                        })
                                    }
                                }} className="page-input" maxLength={2}/>
                                <label> / {this.state.numPages}</label>
                            </div>
                            <div className="pdf-button-nav">
                                <button className="pdf-nav-btn" disabled={(this.state.pageNumber <= 1)} onClick={()=>{
                                    if(this.state.pageNumber > 1){
                                        this.setState({
                                            pageNumber:this.state.pageNumber-1
                                        },()=>{
                                            this.pdfPageCount.value = this.state.pageNumber;
                                            this.scrollingContainer.scrollTop = 0;
                                        })
                                    }
                                }}>Previous</button>
                                <button className="pdf-nav-btn" disabled={(this.state.pageNumber >= this.state.numPages)} onClick={()=>{
                                    if(this.state.pageNumber < this.state.numPages){
                                        this.setState({
                                            pageNumber:this.state.pageNumber+1
                                        },()=>{
                                            this.pdfPageCount.value = this.state.pageNumber;
                                            this.scrollingContainer.scrollTop = 0;
                                        })
                                    }
                                }}>Next</button>
                            </div>
                        </div>
                    </div>
                    :
                    null
                }
            </div>
        )
    }
}