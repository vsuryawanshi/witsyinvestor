import React from "react";
import axios from "axios";

export default class VerifyPage extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            userVerified:false
        }
    }

    componentDidMount(){
        var qs = this.props.location.search.substring(1);
        qs = qs.split("=")[1];
        var urlToVerify = decodeURIComponent(qs);
        this.verifyUser(urlToVerify);
    }

    verifyUser(urlToVerify){
        axios({
            method:"GET",
            url:urlToVerify
        }).then(response => {
            this.setState({
                userVerified:true
            });
        })
    }

    render(){
        return(
            <div className="verify-wrap">
                <div className="container">
                    <h1>User Verification</h1>
                    {
                        this.state.userVerified ? <div className="success">User verified. Please proceed to <a onClick={()=>{
                            this.props.history.replace("/");
                        }}>Login</a></div>
                        :
                        <div>User still not verified</div>
                    }
                </div>
            </div>
        )
    }
}