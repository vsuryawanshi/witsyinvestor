import React, {Component} from "react";

const DATA = [
    {
        botName:"Paper bot",
        botLine:"I find the latest cutting edge research in AI for you",
        botImage:"https://robohash.org/" + Math.random().toString(36).substring(7),
        showBotTime:true,
        witsyLine:"Here are the top 3 trending papers on GAN",
        witsyData:{
            type:"rp",
            arr:[
                {
                    img:require("../../images/rp.png"),
                    text1:"Finding Solutions to GAN related problems...",
                    text2:"27"
                },
                {
                    img:require("../../images/rp.png"),
                    text1:"On Self Modulation for General Adversarial Net...",
                    text2:"23"
                },
                {
                    img:require("../../images/rp.png"),
                    text1:"Generating Transferable Adversarial...",
                    text2:"15"
                }
            ]
        }
    },
    {
        botName:"Annotation Bot",
        botLine:"I provide you with the fastest data annotation tool",
        botImage:"https://robohash.org/" + Math.random().toString(36).substring(7),
        showBotTime:true,
        witsyLine:"Here are some examples of image annotation and tagging tools", 
        animate:true,  
        witsyData:{
            type:"annotation",
            arr:[
                {
                    image:require("../../images/realestate.jpg"),
                    tags:["Pool","Duplex","Green"],
                },
                {
                    image:require("../../images/anno2.png"),
                    tags:["Lung Opacity","Lung Opacity"] 
                }
            ]
        }
    },
    {
        botName:"Papercode Bot",
        botLine:"I find research paper implementation for you",
        botImage:"https://robohash.org/" + Math.random().toString(36).substring(7),
        showBotTime:true,
        witsyLine:"Here are some Popular Github repositories for GAN",   
        witsyData:{
            type:"gh",
            arr:[
                {
                    img:require("../../images/github-logo.png"),
                    text1:"werty/cnnmodelf...",
                    text2:"1035.3322"
                },
                {
                    img:require("../../images/github-logo.png"),
                    text1:"airbnb/tensorflow...",
                    text2:"43322.776"
                },
                {
                    img:require("../../images/github-logo.png"),
                    text1:"vsvanshi/grapdbvis...",
                    text2:"8573.334"
                }    
            ]
        }
    },
    {
        botName:"Researchers Bot",
        botLine:"I let you follow Top Researchers and what they are upto these days",
        botImage:"https://robohash.org/" + Math.random().toString(36).substring(7),
        showBotTime:true,
        witsyLine:"Following are the top 3 researchers and some insights about what they have been doing",   
        witsyData:{
            type:"people",
            arr:[
                {
                    img:require("../../images/r1.jpg"),
                    text1:"M. Jordan",
                    text2:"In the latest #codingtensorflow, we tackle all problems",
                    text3:"mordan/xcode..."
                },
                {
                    img:require("../../images/r2.jpg"),
                    text1:"Terrence S.",
                    text2:"Attending the #tfsummit2018, psyched!!",
                    text3:"tsmor/blob..."
                },
                {
                    img:require("../../images/r3.jpg"),
                    text1:"Elliot M.",
                    text2:"#codingproblems, #stressedup lets take a break",
                    text3:"elliotcode/opti.."
                }    
            ]
        }
    },
    {
        botName:"Events bot",
        botLine:"I also show you the events happening around you",
        botImage:"https://robohash.org/" + Math.random().toString(36).substring(7),
        showBotTime:true,
        witsyLine:"Here are the 3 nearest events happening around you",   
        witsyData:{
            type:"events",
            arr:[
                {
                    img:require("../../images/e1.jpeg"),
                    text1:"Delhi AI/ML Group",
                    text2:"New Delhi, India",
                    text3:"13 Oct, 2018"
                },
                {
                    img:require("../../images/e2.jpeg"),
                    text1:"Amazon Meetup",
                    text2:"Ahmedabad, India",
                    text3:"27,28 Oct, 2018"
                },
                {
                    img:require("../../images/e3.jpeg"),
                    text1:"DataGiri",
                    text2:"Mumbai, India",
                    text3:"27-28 Oct, 2018"
                }    
            ]
        }
    },
    {
        botName:"Data bot",
        botLine:"I let you discover various datasets",
        botImage:"https://robohash.org/" + Math.random().toString(36).substring(7),
        showBotTime:true,
        witsyLine:"Some datasets for you",   
        witsyData:{
            type:"discover_dataset",
            arr:[
                {
                    img:require("../../images/dataset.png"),
                    text1:"Cancer Dataset",
                    text2:"22123 images",
                    tags:["Healthcare","Disease"]
                },
                {
                    img:require("../../images/dataset.png"),
                    text1:"Ocean Dataset",
                    text2:"89232 images",
                    tags:["View","Outdoor"]
                },
                {
                    img:require("../../images/dataset.png"),
                    text1:"Swimming Pool Images",
                    text2:"3213 images",
                    tags:["Property","House"]
                }    
            ]
        }
    },
    {
        botName:"Crawler bot",
        botLine:"Give me images of babies from google",
        botImage:"https://robohash.org/" + Math.random().toString(36).substring(7),
        showBotTime:true,
        witsyLine:"Here are your results",   
        witsyData:{
            type:"create_dataset",
            arr:[
                require("../../images/b1.jpeg"),
                require("../../images/b2.jpeg"),
                require("../../images/b3.jpeg"),
                require("../../images/b4.jpeg"),
                require("../../images/b5.jpeg"),
                require("../../images/b6.jpeg"),
                require("../../images/b7.jpeg"),
                require("../../images/b8.jpeg"),
                require("../../images/b9.jpeg"),
                require("../../images/b10.jpeg"),
                require("../../images/b11.jpeg"),
                require("../../images/b12.jpeg")
            ]
        }
    }
];

export default class AnimatedChatContainer extends Component{
    constructor(props){
        super(props);
        this.state = {
            currentData:DATA[0],
            currentAnnotationData:null
        }

        this.timeout = null;
        this.currentIdx = 0;
    }

    componentDidMount(){
        this.wait(500,()=>{
            this.animateChats();
        });
    }

    hideChatContainer(doneCallback){
        this.chatContainer.style.transform = "translateY(-100px)";
        this.chatContainer.style.opacity = 0;
        this.timeout = setTimeout(()=>{
            this.userChat.classList.remove("animate");
            this.witsyChat.classList.remove("animate");
            doneCallback();
        },100);
    }

    showChatContainer(doneCallback){
        this.chatContainer.style.transform = "translateY(0px)"
        this.chatContainer.style.opacity = 1; 
        this.timeout = setTimeout(() => {
            this.userChat.classList.add("animate");
            this.timeout = setTimeout(()=>{
                this.witsyChat.classList.add("animate");
                doneCallback();
            },800);
        }, 2000);  
    }

    wait(time,callback){
        this.timeout = setTimeout(()=>{
            callback();
        },time);
    }

    animateChats(){
        this.setState({
            currentData:DATA[(this.currentIdx < DATA.length) ? this.currentIdx++ : 0]
        },()=>{
            if(this.currentIdx >= DATA.length) this.currentIdx = 0;
            this.showChatContainer(()=>{
                if(this.state.currentData.animate){
                    this.setState({
                        currentAnnotationData:this.state.currentData.witsyData.arr[0]
                    },()=>{
                        this.firstAnnotationAnimation();
                    });
                } else {
                    this.wait(6000, ()=>{
                        this.hideChatContainer(()=>{
                            this.wait(1000,()=>{
                                this.animateChats();
                            })
                        })
                    })
                }
            })
        });
    }

    firstAnnotationAnimation(){
        this.aimg.style.opacity = 1;
        this.wait(2000,()=>{
            this.atag.style.opacity = 1;
            this.wait(3000,()=>{
                this.aimg.style.opacity = 0;
                this.atag.style.opacity = 0;
                this.wait(1000,()=>{
                    this.secondAnnotationAnimation();
                })
            })
        });
    }

    secondAnnotationAnimation(){
        this.setState({
            currentAnnotationData:this.state.currentData.witsyData.arr[1]
        },()=>{
            this.wait(1000, ()=>{
                this.aimg.style.opacity = 1;  
                this.wait(2000,()=>{
                    this.amask.style.display = "block"; 
                    this.atag.style.opacity = 1;
                    this.wait(3000,()=>{
                        this.hideChatContainer(()=>{
                            this.wait(1000,()=>{
                                this.animateChats();
                            })
                        })
                    })
                })
            })
        })
    }

    componentWillUnmount(){
        clearTimeout(this.timeout);
    }

    render(){
        return(
            <div className="chat-container" ref={node => this.chatContainer = node}>
                <div className="chatbox user" ref={node => this.userChat = node}>
                    <div className="tp">
                        <div className="avatar" style={{backgroundImage:"url(" + this.state.currentData.botImage + ")"}}/>
                        <div className="chatbox-content">
                            <div>
                                <span className="chatBox__meta chatBox__meta--big">{this.state.currentData.botName}</span>
                                {
                                    this.state.currentData.showBotTime ?
                                    <span className="chatBox__meta chatBox__meta--light">{" under 10s"}</span>
                                    :
                                    null
                                }
                            </div>
                            <div className="chatBox__input">{this.state.currentData.botLine}</div>
                        </div>
                    </div>
                </div>
                <div className="chatbox" ref={node => this.witsyChat = node}>
                    <div className="tp">
                        <div className="avatar witsy"/>
                        <div className="chatbox-content">
                            <div>
                                <span className="chatBox__meta chatBox__meta--big">Witsy</span>
                            </div>
                            <div className="chatBox__input">
                                <div className="cb-title">{this.state.currentData.witsyLine}</div>
                            </div>
                        </div>
                    </div>
                    <div className="chatbox-results">
                        {
                            (()=>{
                                switch (this.state.currentData.witsyData.type){
                                    case "rp":
                                        return(
                                            <div>
                                                {
                                                    this.state.currentData.witsyData.arr.map((d,i)=>{
                                                        return(
                                                            <div className="cnt" key={i}>
                                                                <img src={d.img}/>
                                                                <div className="rs">
                                                                    <div className="text1">{d.text1}</div>
                                                                    <div className="text2"><span>hIndex:</span> {d.text2}</div>
                                                                </div>
                                                            </div>  
                                                        )
                                                    })
                                                }
                                            </div>
                                        );
                                    case "gh":
                                        return(
                                            <div className="fw">
                                                {
                                                    this.state.currentData.witsyData.arr.map((d,i)=>{
                                                        return(
                                                            <div className="gh-card" key={i}>
                                                                <img src={d.img}/>
                                                                <div className="right-section">
                                                                    <div className="text1">{d.text1}</div>
                                                                    <div className="text2">
                                                                        <span>Argxiv ID:</span>
                                                                        <br/>
                                                                    {d.text2}</div>
                                                                </div>
                                                            </div>  
                                                        )
                                                    })  
                                                }
                                            </div>
                                        );
                                    case "people":
                                        return(
                                            <div className="fw">
                                                {
                                                    this.state.currentData.witsyData.arr.map((d,i)=>{
                                                        return(
                                                            <div className="peop" key={i}>
                                                                <div style={{backgroundImage:"url(" + d.img + ")"}} className="pp"/>
                                                                <div className="info">
                                                                    <div className="text1">{d.text1}</div>
                                                                    <div className="twi">
                                                                        <img src={require("../../images/twitter.svg")}/>
                                                                        <div className="tt">{d.text2}</div>
                                                                    </div>
                                                                    <div className="gh">
                                                                        <img src={require("../../images/github-logo.png")}/>
                                                                        <div className="tt">{d.text3}</div>
                                                                    </div>
                                                                </div>
                                                            </div>  
                                                        )
                                                    })
                                                }
                                            </div>
                                        );
                                    case "events":
                                        return(
                                            <div className="fw">
                                                {
                                                    this.state.currentData.witsyData.arr.map((d,i)=>{
                                                        return(
                                                            <div className="peop" key={i}>
                                                                <div style={{backgroundImage:"url(" + d.img + ")"}} className="pp"/>
                                                                <div className="info">
                                                                    <div className="text1" style={{marginTop:10}}>{d.text1}</div>
                                                                    <div className="twi">
                                                                        <img src={require("../../images/loc.svg")}/>
                                                                        <div className="tt">{d.text2}</div>
                                                                    </div>
                                                                    <div className="gh">
                                                                        <img src={require("../../images/calendar.svg")}/>
                                                                        <div className="tt n">{d.text3}</div>
                                                                    </div>
                                                                </div>
                                                            </div>  
                                                        )
                                                    })
                                                }
                                            </div>
                                        );
                                    case "discover_dataset":
                                        return(
                                            <div>
                                                {
                                                    this.state.currentData.witsyData.arr.map((d,i)=>{
                                                        return(
                                                            <div className="cnt" key={i}>
                                                                <img src={d.img}/>
                                                                <div className="rs">
                                                                    <div className="text1">{d.text1}</div>
                                                                    <div className="text2">{d.text2}</div>
                                                                </div>
                                                                <div className="tg-cnt">
                                                                    {
                                                                        d.tags.map((tg,ind)=>{
                                                                            return(
                                                                                <div className="tagg" key={ind}>{tg}</div>
                                                                            )
                                                                        })
                                                                    }
                                                                </div>
                                                                <div className="prov">NHS</div>
                                                            </div>  
                                                        )
                                                    })
                                                }       
                                            </div>
                                        )
                                    case "create_dataset":
                                        return (
                                            <div className="fw">
                                                {
                                                    this.state.currentData.witsyData.arr.map((d,i)=>{
                                                        return(
                                                            <img src={d} key={i} className="google-result"/>
                                                        )
                                                    })
                                                }
                                            </div>
                                        )
                                    case "annotation":
                                        if(this.state.currentAnnotationData !== null){
                                            return(
                                                <div className="">
                                                    <div
                                                        ref={node => this.aimg = node} 
                                                        className="image-container" style={{backgroundImage:"url(" +this.state.currentAnnotationData.image + ")"}}>
                                                        <div className="mask" ref={node => this.amask = node}>
                                                            <div className="rect1 r"/>
                                                            <div className="rect2 r"/>
                                                        </div>
                                                        <div className="tag-container" ref={node => this.atag = node}>
                                                            {
                                                                this.state.currentAnnotationData.tags.map((tg,i)=>{
                                                                    return(
                                                                        <div className="qwe" key={i}>{tg}</div>
                                                                    );
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        } else {
                                            return null;
                                        }
                                    default:
                                        return null;
                                }
                            })()
                        }
                    </div>
                </div>
            </div>  
        )
    }
}