import React, {Component} from "react";
import Particles from 'react-particles-js';
import {PARTICLES_PARAMS} from "./hero-section";

export default class CompanyInformation extends Component {
    render(){
        return(
            <section className="company-information black">
                <Particles params={PARTICLES_PARAMS} className="orb-wrap"/>
                <div className="container">
                    <div className="company-wrap">
                        <div className="w25">
                            <h3 className="company-information-header">Company intelligence lives in three places...</h3>
                        </div>
                        <div className="w25 info-location system-apps">
                            <img src={require("../../images/software-apps-icon.svg")} className="company-info-icon"/>
                            <p>System &amp; apps</p>
                            <img src={require("../../images/scattered-line@3x.png")} className="company-info-desktop-line"/>
                        </div>

                        <div className="w25 info-location website-documents">
                            <img src={require("../../images/websites-documents-icon.svg")} className="company-info-icon"/>
                            <p>System &amp; apps</p>
                            <img src={require("../../images/buried-line@3x.png")} className="company-info-desktop-line"/>
                        </div>

                        <div className="w25 info-location tribal-knowledge">
                            <img src={require("../../images/tribal-knowledge-icon.svg")} className="company-info-icon"/>
                            <p>System &amp; apps</p>
                            <img src={require("../../images/uncaptured-line@3x.png")} className="company-info-desktop-line"/>
                        </div>
                    </div>
                    <div className="company-wrap">
                        <div className="w25">
                            <h3 className="each-challenge">...each with its own challenges</h3>
                        </div>
                        <div className="w25 info-location system-apps">
                            <h3 className="what-challenge">Scattered</h3>
                        </div>

                        <div className="w25 info-location website-documents">
                            <h3 className="what-challenge">Buried</h3>
                        </div>

                        <div className="w25 info-location tribal-knowledge">
                            <h3 className="what-challenge">Uncaptured</h3>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}