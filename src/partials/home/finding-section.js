import React, {Component} from "react";

export default class FindingSection extends Component{
    render(){
        return(
            <section className="finding-information black">
                <div className="container">
                    <div className="cards-wrap">
                        <div className="card-container-text w25">
                            <h3>Looking for something? <span>That'll cost you</span></h3>
                            <p className="p-large">Searching for what you need to do your job shouldn't be your job.</p>
                        </div>
                        <div className="card-container w25">
                            <div className="card">
                                <h4>Upto <span>35% of your team's time </span> is wasted searching for information</h4>
                                <div className="card-footer">
                                    <a><img src={require("../../images/km-world-logo@3x.png")} className="card-logo"/></a>
                                </div>
                            </div>
                        </div>
                        <div className="card-container w25">
                            <div className="card">
                                <h4>Upto <span>35% of your team's time </span> is wasted searching for information</h4>
                                <div className="card-footer">
                                    <a><img src={require("../../images/km-world-logo@3x.png")} className="card-logo"/></a>
                                </div>
                            </div>
                        </div>
                        <div className="card-container w25">
                            <div className="card">
                                <h4>Upto <span>35% of your team's time </span> is wasted searching for information</h4>
                                <div className="card-footer">
                                    <a><img src={require("../../images/km-world-logo@3x.png")} className="card-logo"/></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}