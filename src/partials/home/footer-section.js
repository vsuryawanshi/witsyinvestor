import React, {Component} from "react";

export default class FooterSection extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <footer>
                <div className="container">
                    <div className="footer-wrap">
                        <div className="w25">
                            <img className="footer-glasses" src={require("../../images/witsy-single.svg")}/>
                        </div>
                        <div className="w25">
                            <div className="company">
                                <strong>Company</strong>
                                <ul>
                                    <li>
                                        <a>Careers</a>
                                    </li>
                                    <li>
                                        <a>Blog</a>
                                    </li>
                                    <li>
                                        <a>CoPilot Console</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="w25">
                            <div className="get-in-touch">
                                <strong>Get in Touch</strong>
                                <ul>
                                    <li>
                                        <a><img className="social-icon" src={require("../../images/facebook-logo.svg")}/></a>
                                    </li>
                                    <li>
                                        <a><img className="social-icon" src={require("../../images/twitter.svg")}/></a>
                                    </li>
                                </ul>
                                <a  className="hello-button">Say hello <span className="icon-forward"></span></a>
                            </div>
                        </div>

                        <div className="w25">
                            <div className="made-in-stl">
                                <p>
                                    <strong>Made in STL <img src={require("../../images/arch.svg")}/></strong>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}