import React, { Component } from "react";
import Particles from 'react-particles-js';
import AnimatedChatContainer from "./AnimatedChatContainer";

export const PARTICLES_PARAMS = {
    "particles": {
        "number": {
            "value": 60,
            "density": {
                "enable": false,
                "value_area": 1736.124811591
            }
        },
        "color": {
            "value": "#ffffff"
        },
        "shape": {
            "type": "circle",
            "stroke": {
                "width": 0,
                "color": "#000000"
            },
            "polygon": {
                "nb_sides": 5
            },
            "image": {
                "src": "img/github.svg",
                "width": 100,
                "height": 100
            }
        },
        "opacity": {
            "value": 0.5,
            "random": false,
            "anim": {
                "enable": false,
                "speed": 1,
                "opacity_min": 0.1,
                "sync": false
            }
        },
        "size": {
            "value": 5,
            "random": true,
            "anim": {
                "enable": false,
                "speed": 40,
                "size_min": 0.1,
                "sync": false
            }
        },
        "line_linked": {
            "enable": false,
            "distance": 150,
            "color": "#ffffff",
            "opacity": 0.4,
            "width": 1
        },
        "move": {
            "enable": true,
            "speed": 1,
            "direction": "none",
            "random": false,
            "straight": false,
            "out_mode": "out",
            "bounce": false,
            "attract": {
                "enable": false,
                "rotateX": 600,
                "rotateY": 1200
            }
        }
    },
    "interactivity": {
        "detect_on": "canvas",
        "events": {
            "onhover": {
                "enable": false,
                "mode": "repulse"
            },
            "onclick": {
                "enable": false,
                "mode": "push"
            },
            "resize": true
        },
        "modes": {
            "grab": {
                "distance": 400,
                "line_linked": {
                    "opacity": 1
                }
            },
            "bubble": {
                "distance": 400,
                "size": 40,
                "duration": 2,
                "opacity": 8,
                "speed": 3
            },
            "repulse": {
                "distance": 200,
                "duration": 0.4
            },
            "push": {
                "particles_nb": 4
            },
            "remove": {
                "particles_nb": 2
            }
        }
    },
    "retina_detect": true
}


export default class HeroSection extends Component {    
    render() {
        return (
            <section className="hero-area">
                <Particles params={PARTICLES_PARAMS} className="orb-wrap"/>
                <div className="container">
                    <div className="hero-content-container">
                        <h1 className="header-title">Witsy makes Artificial intelligence easy, collaborative and productive in the simplest possible way.</h1>
                        <div className="header-detail-group">
                            <div className="header-detail">
                                Witsy is artificial intelligence platform for Artificial intelligence community
                                <a className="cta-button-large request-a-demo" href="/discover">Explore</a>
                            </div>
                            <AnimatedChatContainer/>
                        </div>
                    </div>
                </div>
                <div className="as-seen-on container">
                    <h4 className="center h4">As seen on:</h4>
                    <div className="comp-wrap">
                        <img src={require("../../images/TechCrunch.png")} className="tech-crunch"/>
                        <img src={require("../../images/VentureBeat.png")} className="venture-beat"/>
                        <img src={require("../../images/Fortune.png")} className="fortune"/>
                        <img src={require("../../images/SXSW.png")} className="sxsw"/>
                    </div>
                </div>
                <div className="hero-video-container">
                    <a>
                        <iframe 
                            width="560" 
                            height="815" 
                            src="https://www.youtube.com/embed/f7dhOHMX0js" 
                            frameBorder="0" 
                            allow="autoplay; encrypted-media" 
                            allowFullScreen=""/>
                    </a>
                </div>
            </section>
        )
    }
}