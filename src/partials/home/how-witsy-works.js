import React, {Component} from "react";

const TABS = [
    "Integrate",
    "Mine",
    "Capture",
    "access"
];

export default class HowWitsyWorksSection extends Component{
    constructor(props){
        super(props);
        this.state = {
            selectedTab:0
        }
    }

    render(){
        return(
            <section className="how-witsy-works">
                <div className="container">
                    <div className="how-witsy-title h3">How Witsy Works</div>
                </div>
                <div className="detail-head-wrapper">
                    <div className="detail-head-container">
                        <ul className="detail-main-tabs">
                        {
                            TABS.map((tb,idx)=>{
                                return(
                                    <li className="detail-tab" key={idx} onClick={()=>{
                                        this.setState({
                                            selectedTab:idx
                                        })
                                    }}>
                                        <a className={"tab" + (this.state.selectedTab == idx ? " active" : "") + (idx == 0 ? " integrate-tab": "") + (idx == 1 ? " mine-tab": "") + (idx == 2 ? " capture-tab": "") + (idx == 3 ? " access-tab": "")}>{tb}</a>
                                    </li>
                                );
                            })
                        }
                        </ul>
                    </div>
                    <div className="container tab-container">
                        The selected tab is {TABS[this.state.selectedTab]}
                    </div>
                </div>
            </section>
        )
    }
}