import React, {Component} from "react";

export default class InfoCapturingSection extends Component{
    render(){
        return(
            <section className="info-capturing-section black">
                <div className="container">
                    <div className="info-capturing">
                        <div className="capture-method system-apps">
                            <div className="capture-method-icon">
                                <img src={require("../../images/software-apps-icon.svg")}/>
                            </div>
                            <h3>
                                Integrating
                                <img src={require("../../images/integrating-horizontal.png")} className="horizontal-line"/>
                            </h3>
                            <p>with your key systems</p>
                        </div>

                        <div className="capture-method website-documents">
                            <div className="capture-method-icon">
                                <img src={require("../../images/websites-documents-icon.svg")}/>
                            </div>
                            <h3>
                                Mining
                                <img src={require("../../images/mining-horizontal.png")} className="horizontal-line"/>
                            </h3>
                            <p>your documents for information</p>
                        </div>

                        <div className="capture-method tribal-knowledge">
                            <div className="capture-method-icon">
                                <img src={require("../../images/tribal-knowledge-icon.svg")}/>
                            </div>
                            <h3>
                                Capturing
                                <img src={require("../../images/capturing-horizontal.png")} className="horizontal-line"/>
                            </h3>
                            <p>your team's tribal knowledge</p>
                        </div>
                    </div>
                    <div className="w50 all-accessible">
                        <h2>...and making it all <span>accessible</span> on any device in natural language</h2>
                    </div>
                </div>
            </section>    
        )
    }
}