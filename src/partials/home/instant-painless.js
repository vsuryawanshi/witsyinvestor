import React, {Component} from "react";

export default class InstantPainlessSection extends Component{
    render(){
        return(
            <section className="instant-painless">
                <div className="container">
                    <div className="tp">
                        <h2>Accessing company information is now</h2>
                        <h1>Instant. Painless. Natural.</h1>
                    </div>
                </div>
                <div className="container witsy-round">
                    <h2>Say hello to the future of AI.</h2>
                    <h3>Say hello to Witsy.</h3>
                    <a className="cta-button-large request-a-demo" id="footer-cta">Request a Demo</a>
                </div>
            </section>
        )
    }
}