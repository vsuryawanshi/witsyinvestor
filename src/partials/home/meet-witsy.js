import React, {Component} from "react";

export default class MeetWitsySection extends Component{
    render(){
        return(
            <section className="meet-witsy">
                <div className="meet-witsy-tri-wrapper">
                    <div className="meet-witsy-top-tri"/>
                    <div className="meet-witsy-bg">
                        <div className="container">
                            <div className="meet-wrap">
                                <div className="w50">
                                    <div className="witsy-left">
                                        <h1>Meet Witsy, your first AI-powered teammate.</h1>
                                        <h2>How does Witsy make all your company's intelligence accessible in the simplest way possible?</h2>
                                    </div>
                                </div>
                                <div className="w50">
                                    <div className="meet-witsy-inner">
                                        <img src={require("../../images/meet-witsy-bg.png")} className="meet-witsy-img"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="meet-witsy-bottom-tri"/>
                </div>
            </section>
        )
    }
}