import React, {Component} from "react";
import Particles from 'react-particles-js';
import {PARTICLES_PARAMS} from "./hero-section";

export default class OvercomeSection extends Component{
    render(){
        return(
            <section className="overcome black">
                <Particles params={PARTICLES_PARAMS} className="orb-wrap"/>
                <div className="container">
                    <div className="overcome-wrap">
                        <h1>How do you overcome these challenges?</h1>
                    </div>
                </div>
            </section>
        )
    }
}