import React, {Component} from "react";
import Particles from 'react-particles-js';
import {PARTICLES_PARAMS} from "./hero-section";

export default class WhyAtWorkSection extends Component{
    render(){
        return(
            <section className="why-at-work black">
                <Particles params={PARTICLES_PARAMS} className="orb-wrap"/>
                <div className="container">
                    <div className="why-wrap">
                        <h1>Why is it so hard to access information at work?</h1>
                    </div>
                </div>
            </section>
        )
    }
}