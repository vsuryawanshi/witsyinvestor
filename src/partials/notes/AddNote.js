import React from "react";
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css'; 
import axios from "axios";
import Constants from "../../common/constants";
import Chips, { Chip } from 'react-chips';
import {getFormattedDateForNote} from "../../common/Utility";
import {unescapeQuotes} from "../../common/Utility";
import UserIcon from "../../common/UserIcon";
import {getUser} from "../../common/LoginUtils";
import {getTags,addTag} from "../../common/Tags";
import CustomToolbar from "../../common/components/CustomEditorToolbar";

const CustomChip = (props) => {
    return(
        <div className="witsy-chip" onClick={()=>{
            props.onRemove();
        }}>{props.value}</div>
    )
}
export default class AddNote extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            editorText:this.props.noteData ? unescapeQuotes(this.props.noteData.body) : "",
            noteTitle:this.props.noteData ? this.props.noteData.title: "",
            tags:this.props.noteData ? this.props.noteData.tags : [],
            mentions:this.props.noteData ? this.props.noteData.mentions : [],
            loginUser:null,
            newTag:"",
            tagAdded:false
        }

        
        this.quillRef = null;      // Quill instance
        this.reactQuillRef = null; // ReactQuill component

        this.imageHandler = this.imageHandler.bind(this);

        this.modules = {
            toolbar: {
                container: "#toolbar",
                handlers: {
                    'image': this.imageHandler
                }
            }
        }

        this.formats = [
            'header',
            'bold', 'italic', 'underline', 'strike', 'blockquote',
            'list', 'bullet', 'indent',
            'link', 'image'
        ]
    }

    componentDidMount() {
        this.attachQuillRefs();
        getUser((ud) => {
            this.setState({
                loginUser:ud
            })
        })
    }
      
    componentDidUpdate() {
        this.attachQuillRefs()
    }

    attachQuillRefs (){
        if (typeof this.reactQuillRef.getEditor !== 'function') return;
        this.quillRef = this.reactQuillRef.getEditor();
    }

    handleChange(value) {
        this.setState({ editorText: value })
    }

    onChipsChange(chips){
        if(this.state.loginUser != null){
            this.setState({
                tags:chips
            })
        }
    }

    addNote(){
        axios({
            method:"POST",
            url:Constants.API_BASE_URL + "/collections/" + this.props.collectionId + "/stickynotes",
            data:{
                "docType": "stickynote",
                "createdBy": this.props.userId,
                "body": this.state.editorText,
                "collectionId": this.props.collectionId,
                "tags": this.state.tags,
                "mentions": this.state.mentions,
                "title":this.state.noteTitle
            }
        }).then(response => {
            this.props.noteAdded();  
        }).catch(err => {
            console.log(err);
            alert("Could not add note");
        })
    }

    updateNote(noteId){
        axios({
            method:"PUT",
            url:Constants.API_BASE_URL + "/collections/" + this.props.collectionId + "/stickynotes/" + noteId,
            data:{
                "docType": "stickynote",
                "createdBy": this.props.userId,
                "body": this.state.editorText,
                "collectionId": this.props.collectionId,
                "tags": this.state.tags,
                "mentions": this.state.mentions,
                "title":this.state.noteTitle
            }
        }).then(response => {
            this.props.noteUpdated();  
        }).catch(err => {
            console.log(err);
            alert("Could not update note");
        })    
    }
    imageHandler(image, callback){
        var _this = this;
        const input = document.createElement('input');

        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.click();
        
        input.onchange = function() {
            const file = input.files[0];
            const formData = new FormData();
        
            formData.append('image', file);
            formData.append('uploadedBy',_this.props.userId);
        
            // Save current cursor state
            const range = _this.quillRef.getSelection(true);
        
            // Insert temporary loading placeholder image
            _this.quillRef.insertEmbed(range.index, 'image', `https://loading.io/spinners/bars/index.progress-bar-facebook-loader.svg`); 
        
            // Move cursor to right side of image (easier to continue typing)
            _this.quillRef.setSelection(range.index + 1);

            axios({
                method:"POST",
                url:Constants.API_BASE_URL + "/files/upload?access_token=" + _this.props.at,
                data:formData
            }).then(response => {
                var imgUrl = Constants.API_BASE_URL + "/files/download/" + response.data.id + "?access_token=" + _this.props.at;
                
                console.log(imgUrl);
                // Remove placeholder image
                _this.quillRef.deleteText(range.index, 1);
            
                // Insert uploaded image
                _this.quillRef.insertEmbed(range.index, 'image', imgUrl); 
            }).catch(err => {
                console.log(err);
            });
        }
    }

    render(){
        return(
            <div className="add-note-wrap">
                <div className="top-section">
                    <div className="title">
                        {
                            this.props.noteData ? "Note" : "Add a new note"
                        }
                    </div>
                    <div className="close-btn" onClick={()=>{
                        this.props.onCloseClicked();
                    }}/>
                    <div className="add-note-content">
                        <div className="nt">
                            <input className="plain-input" placeholder="Add Note Title..." value={this.state.noteTitle} onChange={(e)=>{
                                if(this.state.loginUser !== null){
                                    this.setState({
                                        noteTitle:e.target.value
                                    })
                                }
                            }}/>
                        </div>
                        <div className="editor-wrap">
                            <CustomToolbar />
                            <ReactQuill 
                                readOnly={this.props.readOnly}
                                ref={el =>  this.reactQuillRef = el }
                                value={this.state.editorText} 
                                onChange={this.handleChange.bind(this)}
                                modules={this.modules}
                                formats={this.formats}
                                theme="snow" 
                                placeholder="Please write your note here..."/>
                        </div>

                        
                    </div>
                    <div className="chips-wrap">
                        <div className="chip-title">{this.state.loginUser !== null ? "Add tags" : "Tags"}</div>
                            {
                                this.state.loginUser !== null ?
                                    <Chips
                                        placeholder={"Add tags"}
                                        value={this.state.tags}
                                        fromSuggestionsOnly={true}
                                        fetchSuggestions={(value, callback) => {
                                            getTags(value,callback);
                                        }}
                                        renderSuggestion={(item)=>{
                                            return(
                                                <span>{item.tag}</span>
                                            )
                                        }}
                                        renderChip={(item)=>{
                                            return(
                                                <CustomChip value={item.tag}/>        
                                            )
                                        }}
                                        onChange={this.onChipsChange.bind(this)}/>
                                :
                                <div>
                                    {
                                        this.props.noteData && this.props.noteData.tags.map((nt,idx)=>{
                                            return(
                                                <div className="witsy-chip" key={idx}>{nt.tag}</div>
                                            )
                                        })
                                    }
                                </div>
                            }
                            {
                                this.state.loginUser !== null ?
                                <div style={{display:"flex",justifyContent:"flex-end",marginTop:10}}>
                                    <input type="text" placeholder="Enter a new tag" className="custom-input" value={this.state.newTag} onChange={(e)=>{
                                        this.setState({
                                            newTag:e.target.value
                                        })
                                    }}/>
                                    <button disabled={this.state.newTag == ""} className="btnl lk" style={{marginLeft:10}} onClick={()=>{
                                        addTag(this.state.newTag,(dt,exists)=>{
                                            if(dt == null){
                                                if(exists){
                                                    alert("Tag already exists");
                                                    this.setState({newTag:""})
                                                }
                                            } else{
                                                this.setState({
                                                    tagAdded:true,
                                                    newTag:""
                                                },()=>{
                                                    setTimeout(()=>{
                                                        this.setState({
                                                            tagAdded:false
                                                        })
                                                    },4000);
                                                })
                                            }
                                        })
                                    }}>Add</button>
                                </div>
                                :
                                null
                            }
                            {
                                this.state.tagAdded ?
                                    <span className="add-success">Tag Added</span>
                                :
                                null
                            }
                    </div>
                </div>
                <div className="bottom-section">
                    <div className="left-s">
                        <UserIcon/>
                        <div className="ud">
                            <div className="t">{
                                this.state.loginUser != null ? this.state.loginUser.username : ""
                            }</div>
                            <div className="b">{getFormattedDateForNote(new Date())}</div>
                        </div>
                    </div>
                    <div className="right-s">
                        <button className="save btn" disabled={(this.state.noteTitle == "" || this.state.loginUser == null)} onClick={()=>{
                            this.props.noteData ? this.updateNote(this.props.noteData.id) : this.addNote();
                        }}>{this.props.noteData ? "Update Note" : "Save Note" }</button>
                    </div>
                </div>
            </div>
        )
    }
}